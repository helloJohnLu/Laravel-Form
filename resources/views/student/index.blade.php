@extends('common.layouts')

@section('content')

    @include('common.message')

    <!-- 自定义内容区域 -->
    <div class="panel panel-default">
        <div class="panel-heading">学生列表</div>
        <table class="table table-striped table-hover table-responsive">
            <thead>
            <tr>
                <th>ID</th>
                <th>姓名</th>
                <th>年龄</th>
                <th>性别</th>
                <th>添加时间</th>
                <th width="120">操作</th>
            </tr>
            </thead>
            <tbody>
            @foreach($students as $student)
            <tr>
                <th scope="row">{{ $student->id }}</th>
                <td>{{ $student->name }}</td>
                <td>{{ $student->age }}</td>
                <td>{{ $student->sex }}</td>
                <td>{{ date('Y-m-d', strtotime($student->created_at)) }}</td>
                <td>
                    <a href="/student/{{ $student->id }}">详情</a>
                    <a href="/student/{{ $student->id }}/edit">修改</a>
                    <a href="javascript:;" onclick="del({{ $student->id }})">删除</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <!-- 分页  -->
    <div class="pull-right">
        {{ $students->render() }}
    </div>

    {{-- 删除 --}}
    <script>
        function del(id) {
            $.post('/student/'+id, {'_method':'DELETE','_token':'{{ csrf_token() }}'}, function (data) {
                if(data.status == 1){
                    alert(data.msg);
                    setTimeout("window.location.reload()",500);    // 1 秒后刷新页面
                }else{
                    alert(data.msg);
                }
            },"json");
        }
    </script>

@endsection