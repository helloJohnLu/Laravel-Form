<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    // 学习列表页
    public function index()
    {
        $students = Student::orderBy('id')->paginate(5);

        return view('student.index',compact('students'));
    }

    // 渲染新增表单页面
    public function create()
    {
        return view('student.create');
    }

    // 处理表单提交，写入数据库
    public function store(Request $request)
    {
        // 校验数据合法性
        if ($request->isMethod('POST')) {
/*            // 校验规则
            $this->validate($request,[
                'name' => 'required|min:2|max:10',
                'sex' => 'required',
                'age' => 'required|integer|min:14|max:30',
            ],[
                'required' => ':attribute 为必填项',
                'name.min' => ':attribute 长度不能少于2位字符',
                'name.max' => ':attribute 长度最长不超过10位',
                'age.min' => ':attribute 年龄不能小于14岁',
                'age.max' => ':attribute 年龄不能大于30岁',
                'integer' => ':attribute 必须为整数'
            ],[
                'name' => '姓名',
                'age' => '年龄',
                'sex' => '性别',
            ]);*/

            // 使用 Validator 类验证
            $validator = Validator::make($request->input(),[
                'name' => 'required|min:2|max:10',
                'sex' => 'required',
                'age' => 'required|integer|min:14|max:30',
            ],[
                'required' => ':attribute 为必填项',
                'name.min' => ':attribute 长度不能少于2位字符',
                'name.max' => ':attribute 长度最长不超过10位',
                'age.min' => ':attribute 年龄不能小于14岁',
                'age.max' => ':attribute 年龄不能大于30岁',
                'integer' => ':attribute 必须为整数'
            ],[
                'name' => '姓名',
                'age' => '年龄',
                'sex' => '性别',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
        }else{
            return view('student.create');
        }

        $data = $request->except('_token');    // 接收表单数据

        // 写入数据库
        if(Student::create($data)){
            return redirect('/student')->with('success','新增成功');
        }

        return back()->with('error','新增数据失败！');
    }

    // 渲染更新表单
    public function edit(Student $student)
    {
        return view('student.edit',compact('student'));
    }

    // 处理更新表单提交
    public function update(Student $student,Request $request)
    {
        // 使用 Validator 类验证数据合法性
        $validator = Validator::make($request->input(),[
            'name' => 'required|min:2|max:10',
            'sex' => 'required',
            'age' => 'required|integer|min:14|max:30',
        ],[
            'required' => ':attribute 为必填项',
            'name.min' => ':attribute 长度不能少于2位字符',
            'name.max' => ':attribute 长度最长不超过10位',
            'age.min' => ':attribute 年龄不能小于14岁',
            'age.max' => ':attribute 年龄不能大于30岁',
            'integer' => ':attribute 必须为整数'
        ],[
            'name' => '姓名',
            'age' => '年龄',
            'sex' => '性别',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        // 逻辑
        $student->name = $request->input('name');
        $student->sex = $request->input('sex');
        $student->age = $request->input('age');

        if ($student->save()) {
            return redirect('/student')->with('success','更新数据成功');
        }

        return back()->with('error','更新数据失败！');
    }

    // 学生详情信息
    public function show(Student $student)
    {
        return view('student.detail',compact('student'));
    }

    // 删除数据
    public function destroy(Student $student)
    {
        if ($student->delete()) {
            $data = [
                'status' => 1,
                'msg' => '删除成功'
            ];
        }else{
            $data = [
                'status' => 0,
                'msg' => '删除失败'
            ];
        }
        return $data;
    }
}
